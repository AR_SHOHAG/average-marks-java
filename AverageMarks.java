import java.util.Scanner;

public class AverageMarks {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int i, j, T, N, marks, term = 0, total = 0;
		T = in.nextInt();
		int average[]= new int[T];
		
		for(i=0; i<T; ++i){
			N = in.nextInt();
			for(j=0; j<N; ++j){
				marks = in.nextInt();
				total += marks;
			}
			average[i] = total/N;
			total = 0;
		}
		for(i=0; i<T; ++i){
			System.out.println("Case "+ ++term + ": " + average[i]);
		}
	}

}
